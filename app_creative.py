#!/usr/bin/env python
# coding: utf-8

# In[1]:
import numpy as np
import pandas as pd
import math
import cv2
import os
import time
from psd_tools import PSDImage
from itertools import combinations
from tkinter import *
from tkinter import filedialog,messagebox
from tkinter.ttk import Progressbar
from functions import *
from PIL import Image, ImageEnhance, ImageTk


# In[2]:


def click1():
    textentry1.delete(0,"end")
    filename = filedialog.askdirectory(title="Source Path")
    textentry1.insert(0,filename)


# In[3]:


def click2():
    textentry2.delete(0,"end")
    filename = filedialog.askdirectory(title="Source Path")
    textentry2.insert(0,filename)


# In[4]:


def click3():
    textentry3.delete(0,"end")
    filename = filedialog.askopenfilename(initialdir="/", title="Select A File", filetypes=(("Excel files", "*.xlsx"),("Csv files", "*.csv"),("All files", "*.*")))
    textentry3.insert(0,filename)


# In[5]:


def close():
    global newWindow2
    newWindow2.destroy()


# In[6]:


def sel():
    global v,por
    por.set((v.get()))


# In[7]:


def sel2():
    global r,resolution
    resolution.set((r.get()))


# In[8]:


def sel3():
    global n,asset_name
    asset_name.set((n.get()))

def sel4():
    global e,extn
    extn.set((e.get()))

# In[9]:


def aratio():
    global aspect_ratio,tempWindow,arentry
    aspect_ratio = arentry.get()
    tempWindow.destroy()


# In[10]:


def click():
    global src_path,dst_path,df,creative,creative_name,aspect_ratio,arentry,sets,asset_lst,partial_overlap,lang_lst,lang_dict,v,por,por_lst,counter,length,display,extbtn,tempWindow,newWindow,shape_lst,e,extn
    src_path = textentry1.get()
    dst_path = textentry2.get()
    filename = textentry3.get()
    
    try:
        df = pd.read_excel(filename)
        #reading benchmark creative psd file
        dirname, foldernames, filenames = list(os.walk(src_path))[0]
        for filename in filenames:
            if os.path.splitext(filename)[-1] == '.psd':
                file_name = os.path.splitext(filename)[0]
                psd = PSDImage.open(os.path.join(dirname,filename))
        #getting benchmark creative in numpy format
        creative = np.array(psd.composite())[:,:,:3]
    except:
        messagebox.showerror("ERROR", "Path is incorrect. Please try again")
    else:    
        #parsing creative features to get creative name and aspect ratio
        aspect_ratio = None
        creative_name = file_name.split('-')[1]
        creative_shape = str(creative.shape[1]) + ' x ' + str(creative.shape[0])
        for ar in df.keys():
            for shapes in df[ar]:
                if shapes == creative_shape:
                    aspect_ratio = ar
                    break
            else:
                continue
            break

        #identifying group assets
        asset_lst = []
        for element in psd:
            if element.name != '1' and element.is_visible():
                asset_lst.append(element)

        try:
            #finding language specific assets
            lang_dict = {}
            for element in psd:
                if '-' in element.name and 'disclaimer' not in element.name:
                    name = element.name.split('-')[0]
                    lang = element.name.split('-')[-1]
                    if lang_dict.get(lang,0) == 0:
                        lang_dict[lang]={}
                    lang_dict[lang][name] = element
                    if element.is_visible():
                        bench_lang = lang

            lang_lst = [bench_lang] + [x for x in lang_dict.keys() if x!=bench_lang]
        except:
            lang_lst =[""]

        newasset_lst = asset_lst.copy()
        newasset_lst.reverse()
        
        #finding overlapping assets
        master = []
        partial_overlap = {}
        for i in range(len(newasset_lst)):
            lst = [newasset_lst[i].name]
            partial_overlap[newasset_lst[i].name.split('-')[0]] = []
            for j in range(i+1,len(newasset_lst)):
                if 'disclaimer' not in newasset_lst[i].name and 'disclaimer' not in newasset_lst[j].name:
                    flagA, flagB = overlap_check(newasset_lst[i],newasset_lst[j],creative,thres=0.5)
                    if flagA:
                        lst.append(newasset_lst[j].name)
                    if flagB:
                        partial_overlap[newasset_lst[i].name.split('-')[0]].append(newasset_lst[j].name.split('-')[0])
            master.append(tuple(lst))
            

        sets = [set(x) for x in master]

        stable = False
        while not stable:                        # loop until no further reduction is found
            stable = True
            # iterate over pairs of distinct sets
            for s,t in combinations(sets, 2):
                if s & t:                        # do the sets intersect ?
                    s |= t                       # move items from t to s 
                    t ^= t                       # empty t
                    stable = False

            # remove empty sets
            sets = list(filter(None, sets)) # added list() for python 3

        sets.reverse()
        
        if aspect_ratio is None:
            tempWindow = Toplevel(window)
            tempWindow.configure(background="black")
            Label(tempWindow,text= "Enter creative bucket (aspect ratio)",bg="black",fg="white").pack()
            arentry = Entry(tempWindow,width=30,bg="white")
            arentry.pack()
            Button(tempWindow, text="Submit",width=6,command=aratio).pack()

        por_lst = []
        #fit_flag_lst = []

        newWindow = Toplevel(window)
        #newWindow.geometry("800x200")
        newWindow.title("Inputs")
        por = IntVar()
        v = IntVar()
        counter = 0
        length = len(sets)
        Label(newWindow,text="Point of Resize: Point about which resizing has to be done",bg="black",fg = "white").grid()
        #initialising variable to record point of resize
        values = {"Centroid" : 0,
                  "Bottom Left" : 1,
                  "Top Left" : 2,
                  "Top Right" : 3,
                  "Bottom Right" : 4}
        
        for (text, value) in values.items():
            Radiobutton(newWindow, text = text, variable = v, 
                        value = value, indicator = 0,width=12,selectcolor="white",
                        background = "light blue",command=sel).grid(row=value+1,column=0)

        extbtn = Button(newWindow, text="Next",width=6,command=update)
        extbtn.grid(row=value+2,column=0,sticky=E)
        backbtn = Button(newWindow, text="Back",width=6,command=backward)
        backbtn.grid(row=value+2,column=0,stick=W)
        
        #Selecting the image extension of final image generated
        Label(newWindow,text="SELECT: Image Extension",bg="black",fg = "white").grid(pady=5)
        e = IntVar()
        extn = IntVar()
        values1 = {"JPG" : 0,
                  "PNG" : 1}
        for (text1, value1) in values1.items():
            Radiobutton(newWindow, text = text1, variable = e, 
                        value = value1, command=sel4).grid()

        #adding image to GUI, initialising with the 1st asset
        element = sets[counter]
        assets = [x for x in asset_lst if x.name in element]
        if len(assets)>1:
            asset,bbox,_ = groupasset(assets,creative,0,src_path)
        else:
            int_asset = np.array(Image.open(os.path.join(src_path,assets[0].name+'.png')))
            int_bbox = assets[0].bbox
            int_center = centroid(int_bbox)
            bbox = new_box(int_center,int_asset)
            asset = int_asset[max(0,bbox[1])-bbox[1]:min(creative.shape[0],bbox[3])-bbox[1],
                              max(0,bbox[0])-bbox[0]:min(creative.shape[1],bbox[2])-bbox[0]].copy()
        load = Image.fromarray(asset)
        load.thumbnail((400,400),Image.ANTIALIAS)
        render = ImageTk.PhotoImage(load)
        display = Label(newWindow, image=render)
        display.image = render
        display.grid(row=0,column=1,rowspan=10)


# In[11]:


def update():
    global src_path,dst_path,df,creative,creative_name,aspect_ratio,sets,asset_lst,lang_lst,lang_dict,por_lst,length,counter,display,extbtn,newWindow
    por_lst.append(por.get())
    counter+=1
    if counter < length:
        element = sets[counter]
        assets = [x for x in asset_lst if x.name in element]
        if len(assets)>1:
            asset,_,_ = groupasset(assets,creative,0,src_path)
        else:
            int_asset = np.array(Image.open(os.path.join(src_path,assets[0].name+'.png')))
            int_bbox = assets[0].bbox
            int_center = centroid(int_bbox)
            bbox = new_box(int_center,int_asset)
            asset = int_asset[max(0,bbox[1])-bbox[1]:min(creative.shape[0],bbox[3])-bbox[1],
                              max(0,bbox[0])-bbox[0]:min(creative.shape[1],bbox[2])-bbox[0]].copy()
        load = Image.fromarray(asset)
        load.thumbnail((400,400),Image.ANTIALIAS)
        render = ImageTk.PhotoImage(load)
        display.configure(image=render)
        display.image = render
    else:
        extbtn.configure(text="Submit",command=main)


# In[12]:


def backward():
    global src_path,dst_path,df,creative,creative_name,aspect_ratio,sets,asset_lst,lang_lst,lang_dict,por_lst,length,counter,display,extbtn,newWindow
    counter-=1
    if counter < 0:
        counter+=1
    else:
        por_lst.pop()
        if counter < length:
            element = sets[counter]
            assets = [x for x in asset_lst if x.name in element]
            if len(assets)>1:
                asset,_,_ = groupasset(assets,creative,0,src_path)
            else:
                int_asset = np.array(Image.open(os.path.join(src_path,assets[0].name+'.png')))
                int_bbox = assets[0].bbox
                int_center = centroid(int_bbox)
                bbox = new_box(int_center,int_asset)
                asset = int_asset[max(0,bbox[1])-bbox[1]:min(creative.shape[0],bbox[3])-bbox[1],
                                  max(0,bbox[0])-bbox[0]:min(creative.shape[1],bbox[2])-bbox[0]].copy()
            load = Image.fromarray(asset)
            load.thumbnail((400,400),Image.ANTIALIAS)
            render = ImageTk.PhotoImage(load)
            display.configure(image=render)
            display.image = render
            extbtn.configure(text="Next",command=update)


# In[13]:


def main():
    
    global src_path,dst_path,df,creative,creative_name,aspect_ratio,sets,asset_lst,partial_overlap,lang_lst,lang_dict,por_lst,newWindow,shape_lst,newWindow1,creative_edit,extn
    
    newWindow.destroy()
    
    newWindow1 = Toplevel(window)
    #newWindow1.geometry("800x400")
    newWindow1.title("Progress")
    
    #Image display widget
    load = Image.fromarray(creative)
    load.thumbnail((400,400),Image.ANTIALIAS)
    render = ImageTk.PhotoImage(load)
    display1 = Label(newWindow1,image=render)
    display1.image = render
    display1.pack(pady=20,padx=50)
    
    #shape and language label
    mylabel1 = Label(newWindow1,text= "NULL",bg="black",fg="white")
    mylabel1.pack(pady=10,padx=50)
    
    mylabel2 = Label(newWindow1,text= "NULL",bg="black",fg="white")
    mylabel2.pack(pady=5,padx=50)
    
    # Progress bar widget 
    progress = Progressbar(newWindow1, orient = HORIZONTAL, 
                  length = 400, mode = 'determinate')
    progress.pack(pady=10,padx=50)
    
    #Next button to edit window
    nextbutton = Button(newWindow1, text="Next",width=6,command=select,state="disabled")
    nextbutton.pack(pady=10,padx=50)
    
    messagebox.showinfo("Information", "Creative generation started")
    
    len_res = 0
    for resolution in df[aspect_ratio]:
        if resolution is np.nan:
            break
        len_res+=1
    len_lang = len(lang_lst)
    len_sets = len(sets)
    
    tot_loop = len_res*len_lang*len_sets
    bar_prog = 100/tot_loop
    
    shape_lst = []
    #dict for creative edit in gui
    creative_edit ={}
    
    try:
        for k,resolution in enumerate(df[aspect_ratio]):

            if resolution is np.nan:
                break

            creative_edit[resolution] = {}
            shape_lst.append(resolution)
            shape = (int(resolution.split('x')[0]),int(resolution.split('x')[1]))

            folder_name = creative_name + str(shape[0]) + 'x' + str(shape[1])

            #creating directory
            path1 = dst_path+'/'+folder_name
            path2 = dst_path+'/'+folder_name+'/asset layers'
            if not os.path.exists(path1):
                os.mkdir(path1)
            if not os.path.exists(path2):
                os.mkdir(path2)

            #cropping of background
            bg_int = Image.open(os.path.join(src_path,'1'+'.png'))
            bg = np.array(bg_int)            
            #creating a dictionary of storage of all final creatives
            fin_creatives = {}

            background, reshape = backgroud_crop(bg[:,:,:3],shape,creative)
            creative_edit[resolution]["background"] = background
            backg = Image.fromarray(background)
            backg.thumbnail(shape,Image.ANTIALIAS)
            #saving background asset
            backg.save(path2+'/'+'1'+'.jpg',"JPEG",quality = 95)
            #for tracking center of assets of benchmark language
            asset_center = []

            #pasting other assets in order of their importances
            for i,lang in enumerate(lang_lst):
                fin_creatives['int_img'] = background
                #initialisng mask for mask creation of final creative
                fin_creatives['mask_fin'] = {}
                for j,elements in enumerate(sets):

                    disc = False #bit to identify disclaimer asset
                    psd_asset = [x for x in asset_lst if x.name in elements]
                    assets = []
                    asset_names = []
                    for element in psd_asset:
                        if '-' in element.name and 'disclaimer' not in element.name:
                            name = element.name.split('-')[0]
                            asset = lang_dict[lang][name]
                            asset.visible = True
                        elif 'disclaimer' in element.name:
                            disc = True
                            name = element.name.split('-')[0]
                            asset = element
                        else:
                            name = element.name
                            asset = element
                        assets.append(asset)
                        asset_names.append(name)

                    asset_name = "&".join(asset_names)

                    #for benchmark language only we calculate the centroid of the assets in the original creative
                    if i == 0:
                        if len(assets)>1:
                            asset,bbox,center_lst = groupasset(assets,creative,0,src_path)
                            asset_center.append(center_lst)
                            rect_box = [max(0,bbox[0]),max(0,bbox[1]),
                                        min(creative.shape[1],bbox[2]),min(creative.shape[0],bbox[3])]
                            center = centroid(rect_box)
                        else:
                            int_asset = np.array(Image.open(os.path.join(src_path,assets[0].name+'.png')))
                            if int_asset.shape[2] == 3:
                                int_asset = cv2.cvtColor(int_asset,cv2.COLOR_RGB2RGBA)
                            int_bbox = assets[0].bbox
                            int_center = centroid(int_bbox)
                            bbox = new_box(int_center,int_asset)
                            center_lst = centroid(bbox)
                            asset_center.append(center_lst)
                            asset = int_asset[max(0,bbox[1])-bbox[1]:min(creative.shape[0],bbox[3])-bbox[1],
                                              max(0,bbox[0])-bbox[0]:min(creative.shape[1],bbox[2])-bbox[0]].copy()
                            #find location of asset in benchmark creative
                            rect_box = [max(0,bbox[0]),max(0,bbox[1]),
                                        min(creative.shape[1],bbox[2]),min(creative.shape[0],bbox[3])]
                            center = centroid(rect_box)

                    else:
                        if len(assets)>1:
                            center_lst = asset_center[j]
                            asset,bbox,_ = groupasset(assets,creative,center_lst,src_path)
                            rect_box = [max(0,bbox[0]),max(0,bbox[1]),
                                        min(creative.shape[1],bbox[2]),min(creative.shape[0],bbox[3])]
                            center = centroid(rect_box)
                        else:
                            int_asset = np.array(Image.open(os.path.join(src_path,assets[0].name+'.png')))
                            if int_asset.shape[2] == 3:
                                int_asset = cv2.cvtColor(int_asset,cv2.COLOR_RGB2RGBA)
                            center_lst = asset_center[j]
                            bbox = new_box(center_lst,int_asset)
                            asset = int_asset[max(0,bbox[1])-bbox[1]:min(creative.shape[0],bbox[3])-bbox[1],
                                              max(0,bbox[0])-bbox[0]:min(creative.shape[1],bbox[2])-bbox[0]].copy()
                            #find location of asset in benchmark creative
                            rect_box = [max(0,bbox[0]),max(0,bbox[1]),
                                        min(creative.shape[1],bbox[2]),min(creative.shape[0],bbox[3])]
                            center = centroid(rect_box)


                    #track its centroid in the reshaped creative
                    new_center = centroid_track(center,creative,reshape)

                    #condition for over ruling fit command
                    flag = [1,0]
                    if ((rect_box[2]-rect_box[0])!=creative.shape[1]):
                        flag[0] = 0
                    if ((rect_box[3]-rect_box[1])!=creative.shape[0]):
                        flag[1] = 0

                    names = []
                    for name in asset_names:
                        names += partial_overlap[name]

                    tot_mask = np.zeros_like(background,dtype=np.float32)
                    for key,value in fin_creatives['mask_fin'].items():
                        a = set(key.split('&'))
                        if len(a.intersection(names)) == 0:
                            tot_mask = cv2.bitwise_or(tot_mask,value)
                            tot_mask[tot_mask<1] = 0.0
                            tot_mask[tot_mask>=1] = 1.0

                    if asset_name == 'logo':
                        img, bo = bg2ele_checknresize(new_center,asset,background,creative,
                                                      margin=0.01,point_of_resize=por_lst[j],fit=flag)
                    else:
                        img, bo = bg2ele_checknresize(new_center,asset,background,creative,
                                                      point_of_resize=por_lst[j],fit=flag)
                        
                    if not disc:
                        img, bo = ele2ele_checknresize(img,bo,new_center,background,
                                                       tot_mask,point_of_resize=por_lst[j])


                    #disintegrating group assets
                    if len(assets)>1:
                        new_w = math.ceil((bo[2]-bo[0])/(rect_box[2]-rect_box[0])*(bbox[2]-bbox[0]))
                        new_h = round((new_w/(bbox[2]-bbox[0]))*(bbox[3]-bbox[1]))
                        #getting full asset bounding box
                        left = bo[0] - round(((rect_box[0]-bbox[0])/(bbox[2]-bbox[0]))*new_w)
                        top = bo[1] - round(((rect_box[1]-bbox[1])/(bbox[3]-bbox[1]))*new_h)
                        right = left + new_w
                        bottom = top + new_h
                        full_box = [left,top,right,bottom]
                        for num,value in enumerate(center_lst):
                            child_asset = np.array(Image.open(os.path.join(src_path,assets[num].name+'.png')))
                            child_box = new_box(center_lst[num],child_asset)
                            #getting new child image
                            new_w = math.ceil((child_asset.shape[1]/(bbox[2]-bbox[0]))*(full_box[2]-full_box[0]))
                            new_h = round((new_w/child_asset.shape[1])*child_asset.shape[0])
                            child_img = Image.fromarray(child_asset)
                            child_img.thumbnail((new_w,new_h),Image.ANTIALIAS)
                            child_img = np.array(child_img)
                            #getting new child box
                            left = full_box[0] + round(((child_box[0]-bbox[0])/(bbox[2]-bbox[0]))*(full_box[2]-full_box[0]))
                            top = full_box[1] + round(((child_box[1]-bbox[1])/(bbox[3]-bbox[1]))*(full_box[3]-full_box[1]))
                            right = left + child_img.shape[1]
                            bottom = top + child_img.shape[0]
                            child_bo = [left,top,right,bottom]
                            #storing asset information for gui edit
                            if i == 0:
                                creative_edit[resolution][asset_names[num]] = [[child_img,child_asset],child_bo]
                            else:
                                creative_edit[resolution][asset_names[num]].append([child_img,child_asset])
                                
                            ext_asset, int_mask = extract_asset(child_img)
                            overlay,mask,asset_layer = create_overlaynmask(background,ext_asset,int_mask,child_bo)
                            #pasting assets
                            fin_creatives['int_img'] = paste_asset(overlay,mask,fin_creatives['int_img'])
                            if num == 0:
                                mask_int = mask
                            else:
                                mask_int = cv2.bitwise_or(mask_int,mask)
                                mask_int[mask_int<1] = 0.0
                                mask_int[mask_int>=1] = 1.0
                            
                            #saving assets as a layer
                            path3 = path2+'/'+lang
                            if not os.path.exists(path3):
                                os.mkdir(path3)
                            asset_layerint = Image.fromarray(asset_layer)
                            asset_layerint.thumbnail(shape,Image.ANTIALIAS)
                            enh_sha = ImageEnhance.Sharpness(asset_layerint)
                            sharpness = 1.0
                            image_sharped = enh_sha.enhance(sharpness)
                            image_sharped.save(path3+'/'+asset_names[num]+'.png',"PNG",quality=95)
                        #creating mask of all assets at one place
                        fin_creatives['mask_fin'][asset_name] = mask_int
                    else:
                        new_w = math.ceil((bo[2]-bo[0])/(rect_box[2]-rect_box[0])*(bbox[2]-bbox[0]))
                        new_h = round((new_w/int_asset.shape[1])*int_asset.shape[0])
                        full_asset = Image.fromarray(int_asset)
                        full_asset.thumbnail((new_w,new_h),Image.ANTIALIAS)
                        full_asset = np.array(full_asset)
                        #getting full asset bounding box
                        left = bo[0] - round(((rect_box[0]-bbox[0])/(bbox[2]-bbox[0]))*full_asset.shape[1])
                        top = bo[1] - round(((rect_box[1]-bbox[1])/(bbox[3]-bbox[1]))*full_asset.shape[0])
                        right = left + full_asset.shape[1]
                        bottom = top + full_asset.shape[0]
                        full_box = [left,top,right,bottom]
                        #storing asset information for gui edit
                        if i == 0:
                            creative_edit[resolution][asset_name] = [[full_asset,int_asset],full_box]
                        else:
                            creative_edit[resolution][asset_name].append([full_asset,int_asset])
                        
                        ext_asset, int_mask = extract_asset(full_asset)
                        overlay,mask,asset_layer = create_overlaynmask(background,ext_asset,int_mask,full_box)
                        #creating mask of all assets at one place
                        fin_creatives['mask_fin'][asset_name] = mask
                        #pasting assets
                        fin_creatives['int_img'] = paste_asset(overlay,mask,fin_creatives['int_img'])
                        
                        #saving assets as a layer
                        path3 = path2+'/'+lang
                        if not os.path.exists(path3):
                            os.mkdir(path3)
                        asset_layerint = Image.fromarray(asset_layer)
                        asset_layerint.thumbnail(shape,Image.ANTIALIAS)
                        enh_sha = ImageEnhance.Sharpness(asset_layerint)
                        sharpness = 1.0
                        image_sharped = enh_sha.enhance(sharpness)
                        image_sharped.save(path3+'/'+asset_name+'.png',"PNG",quality=95)


                    #updating progress bar and image
                    progress["value"] += bar_prog
                    load = Image.fromarray(fin_creatives['int_img'])
                    load.thumbnail((400,400),Image.ANTIALIAS)
                    render = ImageTk.PhotoImage(load)
                    display1.configure(image=render)
                    display1.image = render
                    mylabel1.configure(text="SHAPE: "+resolution)
                    mylabel2.configure(text="LANGUAGE: "+lang)
                    window.update()


                #saving the final resized creative post processing
                path4 = path1+'/'+lang
                if not os.path.exists(path4):
                    os.mkdir(path4)
                int_img1 = Image.fromarray(fin_creatives['int_img'])
                int_img1.thumbnail(shape,Image.ANTIALIAS)
                enh_sha = ImageEnhance.Sharpness(int_img1)
                sharpness = 1.0
                image_sharped = enh_sha.enhance(sharpness)
    #            if showCreative:
    #                image_sharped.show(title="sharp_image")
                if extn.get() == 0:
                    image_sharped.save(path4+"/final_asset.jpg","JPEG",quality=95)
                else:
                    image_sharped.save(path4+"/final_asset.png","PNG",quality=95)
    except:
        messagebox.showerror("ERROR", "Creative generation is unsuccessful")
    else:
        nextbutton.configure(state="normal")
        messagebox.showinfo("SUCCESS!!", "Creative generation is successfully completed")


# In[14]:


def select():
    
    global shape_lst,newWindow1,r,resolution,creative_edit,newWindow2
    newWindow1.destroy()
    newWindow2 = Toplevel(window)
    #newWindow2.geometry("450x800")
    ###################################################
    def on_configure(event):
        # update scrollregion after starting 'mainloop'
        # when all widgets are in canvas
        canvas.configure(scrollregion=canvas.bbox('all'))

    # --- create canvas with scrollbar ---

    canvas = Canvas(newWindow2,width=410,height=800)
    canvas.pack(side=LEFT)

    scrollbar = Scrollbar(newWindow2, command=canvas.yview)
    scrollbar.pack(side=RIGHT, fill='y')

    canvas.configure(yscrollcommand = scrollbar.set)

    # update scrollregion after starting 'mainloop'
    # when all widgets are in canvas
    canvas.bind('<Configure>', on_configure)

    # --- put frame in canvas ---

    frame = Frame(canvas)
    canvas.create_window((0,0), window=frame, anchor='nw')

    # --- add widgets in frame ---
    Label(frame,text= "Selct the Resolution you want to EDIT",bg="black",fg="white").pack(pady=10)
    r = StringVar()
    resolution = StringVar()
    for j,shape in enumerate(shape_lst):
        Radiobutton(frame, text = shape, variable = r, 
                    value = shape, indicator = 0,width=12,selectcolor="white",
                    background = "light blue",command=sel2).pack()
        #asset building
        backg = creative_edit[shape]["background"]
        asset_int = backg.copy()
        for i,name in enumerate(creative_edit[shape].keys()):
            if i != 0:
                img = creative_edit[shape][name][0][0]
                bo = creative_edit[shape][name][1]
                ext_asset, int_mask = extract_asset(img)
                overlay,mask,asset_layer = create_overlaynmask(backg,ext_asset,int_mask,bo)
                #pasting assets
                asset_int = paste_asset(overlay,mask,asset_int)
        h,w,_ = asset_int.shape
        new_w = 400
        new_h = int((new_w/w)*h)
        load = Image.fromarray(asset_int)
        load = load.resize((new_w,new_h))
        render = ImageTk.PhotoImage(load)
        display2 = Label(frame,image=render)
        display2.image = render
        display2.pack()
    editbutton = Button(frame, text="Edit",width=6,command=edit)
    editbutton.pack(pady=10)
    #Exit button
    Button(frame, text="Exit",width=6,command=close).pack(pady=10)


# In[15]:


def edit():
    
    global resolution,creative_edit,n,asset_name,display2,stepsentry,resizentry,ttop,tbottom,tleft,tright,tresize,newWindow3
    #initialising reset variables
    ttop ={}
    tbottom ={}
    tleft ={}
    tright ={}
    tresize = {}
    #initialising asset selection variables
    n = StringVar()
    asset_name = StringVar()
    #imglabel_lst = []
    newWindow3 = Toplevel(window)
    newWindow3.title("Creative Edit")
    Label(newWindow3,text= "EDIT: " + resolution.get(),bg="black",fg="white").grid(row=0,column=1)
    for i,name in enumerate(creative_edit[resolution.get()].keys()):
        if i != 0:
            Radiobutton(newWindow3, variable = n, text= name,
                        value = name, indicator = 0,width=8,selectcolor="white",
                        background = "light blue",command=sel3).grid(row=i,column=1,sticky=W)
            img = creative_edit[resolution.get()][name][0][0]
            h,w,_ = img.shape
            new_h = 20
            new_w = int((new_h/h)*w)
            if new_w > 80:
                new_w = 80
                new_h = int((new_w/w)*h)
            load = Image.fromarray(img)
            load = load.resize((new_w,new_h))
            render = ImageTk.PhotoImage(load)
            imglabel = Label(newWindow3,image=render)
            imglabel.image = render
            imglabel.grid(row=i,column=1,sticky=E)
            ttop[name] = 0
            tbottom[name] = 0
            tleft[name] = 0
            tright[name] = 0
            tresize[name] = 0
    #Add/Delete layer Button
    Button(newWindow3, text="Add",width=6,command=add).grid(row=i+1,column=0,sticky=E)
    Button(newWindow3, text="Up",width=6,command=layer_up).grid(row=i+1,column=1,sticky=W)
    Button(newWindow3, text="Down",width=6,command=layer_down).grid(row=i+1,column=1,sticky=E)
    Button(newWindow3, text="Delete",width=6,command=delete).grid(row=i+1,column=2,sticky=W)
    #Adjustmen step entry
    Label(newWindow3,text= "Adjustment steps (in pixels)",bg="black",fg="white").grid(row=i+2,column=1)
    stepsentry = Entry(newWindow3,width=4,bg="white")
    stepsentry.grid(row=i+3,column=1)
    stepsentry.insert(0,"5")
    #Resize entry
    Label(newWindow3,text= "Resize steps:").grid(row=i+4,column=0,sticky=E)
    resizentry = Entry(newWindow3,width=4,bg="white")
    resizentry.grid(row=i+4,column=1)
    resizentry.insert(0,"+5")
    Button(newWindow3, text="Resize",width=6,command=resize).grid(row=i+4,column=2,sticky=W)
    #Movement buttons
    Button(newWindow3, text="Up",width=6,command=up).grid(row=i+5,column=1)
    Button(newWindow3, text="Down",width=6,command=down).grid(row=i+9,column=1)
    Button(newWindow3, text="Left",width=6,command=left).grid(row=i+7,column=0)
    Button(newWindow3, text="Right",width=6,command=right).grid(row=i+7,column=2)
    #Reset button
    Button(newWindow3, text="Reset",width=6,command=reset).grid(row=i+10,column=1)
    #Save button to save the changes across languages
    Button(newWindow3, text="Save",width=8,command=save).grid(row=i+11,column=1)
    #asset building
    backg = creative_edit[resolution.get()]["background"]
    asset_int = backg.copy()
    for i,name in enumerate(creative_edit[resolution.get()].keys()):
        if i != 0:
            img = creative_edit[resolution.get()][name][0][0]
            bo = creative_edit[resolution.get()][name][1]
            ext_asset, int_mask = extract_asset(img)
            overlay,mask,asset_layer = create_overlaynmask(backg,ext_asset,int_mask,bo)
            #pasting assets
            asset_int = paste_asset(overlay,mask,asset_int)
    load = Image.fromarray(asset_int)
    load.thumbnail((600,600),Image.ANTIALIAS)
    render = ImageTk.PhotoImage(load)
    display2 = Label(newWindow3,image=render)
    display2.image = render
    display2.grid(row=0,column=3,rowspan=len(creative_edit[resolution.get()].keys())+11)
    


# In[16]:

def layer_up():
    global creative_edit,asset_name,resolution,display2,newWindow3
    tmp_lst = list(creative_edit[resolution.get()].keys())
    ind = tmp_lst.index(asset_name.get())
    if ind != 1:
        ind_nxt = ind-1
        tmp = tmp_lst[ind_nxt]
        tmp_lst[ind_nxt] = asset_name.get()
        tmp_lst[ind] = tmp
        tmp_dict = {}
        for key in tmp_lst:
            tmp_dict[key] = creative_edit[resolution.get()][key]
        creative_edit[resolution.get()] = tmp_dict
        newWindow3.destroy()
        edit()
    
def layer_down():
    global creative_edit,asset_name,resolution,display2,newWindow3
    tmp_lst = list(creative_edit[resolution.get()].keys())
    ind = tmp_lst.index(asset_name.get())
    if ind != len(tmp_lst)-1:
        ind_nxt = ind+1
        tmp = tmp_lst[ind_nxt]
        tmp_lst[ind_nxt] = asset_name.get()
        tmp_lst[ind] = tmp
        tmp_dict = {}
        for key in tmp_lst:
            tmp_dict[key] = creative_edit[resolution.get()][key]
        creative_edit[resolution.get()] = tmp_dict
        newWindow3.destroy()
        edit()

def add():
    global creative_edit,resolution,display2,lang_lst,newWindow3
    filename = filedialog.askopenfilename(initialdir="/", title="Select A File", filetypes=(("PNG files", "*.png"),("JPEG files", "*.jpg"),("All files", "*.*")))
    name = filename.split('/')[-1]
    name = os.path.splitext(name)[0]+'-new'
    asset = np.array(Image.open(filename))
    if asset.shape[2] == 3:
        asset = cv2.cvtColor(asset,cv2.COLOR_RGB2RGBA)
    h,w,_ = asset.shape
    left = 0
    top = 0
    right = w
    bottom = h
    box = [left,top,right,bottom]
    for i in range(len(lang_lst)):
        if i == 0:
            creative_edit[resolution.get()][name] = [[asset,asset],box]
        else:
            creative_edit[resolution.get()][name].append([asset,asset])
    newWindow3.destroy()
    edit()
            

def delete():
    global creative_edit,asset_name,resolution,display2
    creative_edit[resolution.get()].pop(asset_name.get())
    newWindow3.destroy()
    edit()


def up():
    global creative_edit,asset_name,resolution,display2,stepsentry,ttop,tbottom
    step = int(stepsentry.get())
    ttop[asset_name.get()] -=step
    tbottom[asset_name.get()] -=step
    box = creative_edit[resolution.get()][asset_name.get()][1]
    [left,top,right,bottom] = box
    top -=step
    bottom -= step
    box = [left,top,right,bottom]
    creative_edit[resolution.get()][asset_name.get()][1] = box
    #asset building
    backg = creative_edit[resolution.get()]["background"]
    asset_int = backg.copy()
    for i,name in enumerate(creative_edit[resolution.get()].keys()):
        if i != 0:
            img = creative_edit[resolution.get()][name][0][0]
            bo = creative_edit[resolution.get()][name][1]
            ext_asset, int_mask = extract_asset(img)
            overlay,mask,asset_layer = create_overlaynmask(backg,ext_asset,int_mask,bo)
            #pasting assets
            asset_int = paste_asset(overlay,mask,asset_int)
    load = Image.fromarray(asset_int)
    load.thumbnail((600,600),Image.ANTIALIAS)
    render = ImageTk.PhotoImage(load)
    display2.configure(image=render)
    display2.image = render
    
def down():
    global creative_edit,asset_name,resolution,display2,stepsentry,ttop,tbottom
    step = int(stepsentry.get())
    ttop[asset_name.get()] +=step
    tbottom[asset_name.get()] +=step
    box = creative_edit[resolution.get()][asset_name.get()][1]
    [left,top,right,bottom] = box
    top +=step
    bottom += step
    box = [left,top,right,bottom]
    creative_edit[resolution.get()][asset_name.get()][1] = box
    #asset building
    backg = creative_edit[resolution.get()]["background"]
    asset_int = backg.copy()
    for i,name in enumerate(creative_edit[resolution.get()].keys()):
        if i != 0:
            img = creative_edit[resolution.get()][name][0][0]
            bo = creative_edit[resolution.get()][name][1]
            ext_asset, int_mask = extract_asset(img)
            overlay,mask,asset_layer = create_overlaynmask(backg,ext_asset,int_mask,bo)
            #pasting assets
            asset_int = paste_asset(overlay,mask,asset_int)
    load = Image.fromarray(asset_int)
    load.thumbnail((600,600),Image.ANTIALIAS)
    render = ImageTk.PhotoImage(load)
    display2.configure(image=render)
    display2.image = render
    
def left():
    global creative_edit,asset_name,resolution,display2,stepsentry,tleft,tright
    step = int(stepsentry.get())
    tleft[asset_name.get()] -=step
    tright[asset_name.get()] -=step
    box = creative_edit[resolution.get()][asset_name.get()][1]
    [left,top,right,bottom] = box
    left -=step
    right -= step
    box = [left,top,right,bottom]
    creative_edit[resolution.get()][asset_name.get()][1] = box
    #asset building
    backg = creative_edit[resolution.get()]["background"]
    asset_int = backg.copy()
    for i,name in enumerate(creative_edit[resolution.get()].keys()):
        if i != 0:
            img = creative_edit[resolution.get()][name][0][0]
            bo = creative_edit[resolution.get()][name][1]
            ext_asset, int_mask = extract_asset(img)
            overlay,mask,asset_layer = create_overlaynmask(backg,ext_asset,int_mask,bo)
            #pasting assets
            asset_int = paste_asset(overlay,mask,asset_int)
    load = Image.fromarray(asset_int)
    load.thumbnail((600,600),Image.ANTIALIAS)
    render = ImageTk.PhotoImage(load)
    display2.configure(image=render)
    display2.image = render
    
def right():
    global creative_edit,asset_name,resolution,display2,stepsentry,tleft,tright
    step = int(stepsentry.get())
    tleft[asset_name.get()] +=step
    tright[asset_name.get()] +=step
    box = creative_edit[resolution.get()][asset_name.get()][1]
    [left,top,right,bottom] = box
    left +=step
    right += step
    box = [left,top,right,bottom]
    creative_edit[resolution.get()][asset_name.get()][1] = box
    #asset building
    backg = creative_edit[resolution.get()]["background"]
    asset_int = backg.copy()
    for i,name in enumerate(creative_edit[resolution.get()].keys()):
        if i != 0:
            img = creative_edit[resolution.get()][name][0][0]
            bo = creative_edit[resolution.get()][name][1]
            ext_asset, int_mask = extract_asset(img)
            overlay,mask,asset_layer = create_overlaynmask(backg,ext_asset,int_mask,bo)
            #pasting assets
            asset_int = paste_asset(overlay,mask,asset_int)
    load = Image.fromarray(asset_int)
    load.thumbnail((600,600),Image.ANTIALIAS)
    render = ImageTk.PhotoImage(load)
    display2.configure(image=render)
    display2.image = render


# In[17]:

def resize():
    global creative_edit,asset_name,resolution,display2,resizentry,tresize
    img = creative_edit[resolution.get()][asset_name.get()][0][0]
    asset = creative_edit[resolution.get()][asset_name.get()][0][1]
    box = creative_edit[resolution.get()][asset_name.get()][1]
    h,w,_ = img.shape
    if '+' in resizentry.get():
        step = int(resizentry.get().split('+')[-1])
        new_w = w + step
    if '-' in resizentry.get():
        step = int(resizentry.get().split('-')[-1])
        new_w = w - step
    new_h = round((new_w/w)*h)
    int_asset = Image.fromarray(asset)
    int_asset = int_asset.resize((new_w,new_h),Image.ANTIALIAS)
    new_w,new_h = int_asset.size
    tresize[asset_name.get()] += new_w - w
    int_asset = np.array(int_asset)
    center = centroid(box)
    bo = new_box(center,int_asset)
    creative_edit[resolution.get()][asset_name.get()][0][0] = int_asset
    creative_edit[resolution.get()][asset_name.get()][1] = bo
    #asset building
    backg = creative_edit[resolution.get()]["background"]
    asset_int = backg.copy()
    for i,name in enumerate(creative_edit[resolution.get()].keys()):
        if i != 0:
            img = creative_edit[resolution.get()][name][0][0]
            bo = creative_edit[resolution.get()][name][1]
            ext_asset, int_mask = extract_asset(img)
            overlay,mask,asset_layer = create_overlaynmask(backg,ext_asset,int_mask,bo)
            #pasting assets
            asset_int = paste_asset(overlay,mask,asset_int)
    load = Image.fromarray(asset_int)
    load.thumbnail((600,600),Image.ANTIALIAS)
    render = ImageTk.PhotoImage(load)
    display2.configure(image=render)
    display2.image = render


def reset():
    global creative_edit,resolution,display2,ttop,tbottom,tleft,tright,tresize
    for i,name in enumerate(creative_edit[resolution.get()].keys()):
        if i != 0:
            img = creative_edit[resolution.get()][name][0][0]
            asset = creative_edit[resolution.get()][name][0][1]
            box = creative_edit[resolution.get()][name][1]
            #box correction
            [left,top,right,bottom] = box
            left -= tleft[name]
            tleft[name] = 0
            top -= ttop[name]
            ttop[name] = 0
            right -= tright[name]
            tright[name] = 0
            bottom -= tbottom[name]
            tbottom[name] = 0
            box = [left,top,right,bottom]
            creative_edit[resolution.get()][name][1] = box
            #resize correction
            h,w,_ = img.shape
            new_w = w - tresize[name]
            tresize[name] = 0
            new_h = round((new_w/w)*h)
            int_asset = Image.fromarray(asset)
            int_asset.thumbnail((new_w,new_h),Image.ANTIALIAS)
            int_asset = np.array(int_asset)
            center = centroid(box)
            bo = new_box(center,int_asset)
            creative_edit[resolution.get()][name][0][0] = int_asset
            creative_edit[resolution.get()][name][1] = bo
            
    #asset building
    backg = creative_edit[resolution.get()]["background"]
    asset_int = backg.copy()
    for i,name in enumerate(creative_edit[resolution.get()].keys()):
        if i != 0:
            img = creative_edit[resolution.get()][name][0][0]
            bo = creative_edit[resolution.get()][name][1]
            ext_asset, int_mask = extract_asset(img)
            overlay,mask,asset_layer = create_overlaynmask(backg,ext_asset,int_mask,bo)
            #pasting assets
            asset_int = paste_asset(overlay,mask,asset_int)
    load = Image.fromarray(asset_int)
    load.thumbnail((600,600),Image.ANTIALIAS)
    render = ImageTk.PhotoImage(load)
    display2.configure(image=render)
    display2.image = render


# In[18]:


def save():
    global creative_edit,resolution,lang_lst,creative_name,dst_path,extn,newWindow2,newWindow3
    
    #newWindow4 = Toplevel(window)
    #newWindow4.title("Saving..")
    #newWindow4.geometry("400x10")
    # Progress bar widget 
    #progress = Progressbar(newWindow4, orient = HORIZONTAL, 
    #              length = 400, mode = 'determinate')
    #progress.pack(pady=10,padx=20)
    try:
        len_lang = len(lang_lst)
        len_sets = len(creative_edit[resolution.get()].keys())-1

        tot_loop = len_lang*len_sets
        bar_prog = 100/tot_loop

        shape = (int(resolution.get().split('x')[0]),int(resolution.get().split('x')[1]))
        folder_name = creative_name + str(shape[0]) + 'x' + str(shape[1])
        #creating directory
        path1 = dst_path+'/'+folder_name
        path2 = dst_path+'/'+folder_name+'/asset layers'

        for i,lang in enumerate(lang_lst):
            backg = creative_edit[resolution.get()]["background"]
            asset_int = backg.copy()
            if i != 0:
                for j,name in enumerate(creative_edit[resolution.get()].keys()):
                    if j != 0:
                        box = creative_edit[resolution.get()][name][1]
                        center = centroid(box)
                        img = creative_edit[resolution.get()][name][i+1][0]
                        bo = new_box(center,img)
                        ext_asset, int_mask = extract_asset(img)
                        overlay,mask,asset_layer = create_overlaynmask(backg,ext_asset,int_mask,bo)
                        #pasting assets
                        asset_int = paste_asset(overlay,mask,asset_int)
                        #saving assets as a layer
                        path3 = path2+'/'+lang
                        if not os.path.exists(path3):
                            os.mkdir(path3)
                        asset_layerint = Image.fromarray(asset_layer)
                        asset_layerint.thumbnail(shape,Image.ANTIALIAS)
                        enh_sha = ImageEnhance.Sharpness(asset_layerint)
                        sharpness = 1.0
                        image_sharped = enh_sha.enhance(sharpness)
                        image_sharped.save(path3+'/'+ 'mod_' +name+'.png',"PNG",quality=95)
                        #updating progress bar and image
                        #progress["value"] += bar_prog
                        #newWindow4.update()
            else:
                for j,name in enumerate(creative_edit[resolution.get()].keys()):
                    if j != 0:
                        img = creative_edit[resolution.get()][name][0][0]
                        bo = creative_edit[resolution.get()][name][1]
                        ext_asset, int_mask = extract_asset(img)
                        overlay,mask,asset_layer = create_overlaynmask(backg,ext_asset,int_mask,bo)
                        #pasting assets
                        asset_int = paste_asset(overlay,mask,asset_int)
                        path3 = path2+'/'+lang
                        if not os.path.exists(path3):
                            os.mkdir(path3)
                        asset_layerint = Image.fromarray(asset_layer)
                        asset_layerint.thumbnail(shape,Image.ANTIALIAS)
                        enh_sha = ImageEnhance.Sharpness(asset_layerint)
                        sharpness = 1.0
                        image_sharped = enh_sha.enhance(sharpness)
                        image_sharped.save(path3+'/'+ 'mod_' +name+'.png',"PNG",quality=95)
                        #progress["value"] += bar_prog
                        #newWindow4.update()

            #saving the final resized creative post processing
            path4 = path1+'/'+lang
            if not os.path.exists(path4):
                os.mkdir(path4)
            int_img1 = Image.fromarray(asset_int)
            int_img1.thumbnail(shape,Image.ANTIALIAS)
            enh_sha = ImageEnhance.Sharpness(int_img1)
            sharpness = 1.0
            image_sharped = enh_sha.enhance(sharpness)
            if extn.get() == 0:
                image_sharped.save(path4+"/final_asset_mod.jpg","JPEG",quality=95)
            else:
                image_sharped.save(path4+"/final_asset_mod.png","PNG",quality=95)
            
            newWindow3.destroy()
    except:
        messagebox.showerror("ERROR", "Changes not saved!!")
    else:
        messagebox.showinfo("SUCCESS!!", "Changes saved Successfully")
        newWindow2.destroy()
        select()
    #newWindow4.destroy()


# In[19]:


window = Tk()
#window.geometry()


# In[20]:


window.title("Creative Adaptation")
window.configure(background="black")


# In[21]:


Label(window,text= "Select source path",bg="black",fg="white").grid(columnspan=2)
textentry1 = Entry(window,width=50,bg="white")
textentry1.grid(row=1,column=0,sticky=E,pady=10)
Button(window, text="Browse",width=6,command=click1).grid(row=1,column=1,sticky=E,pady=10)


# In[22]:


Label(window,text= "Select destination path",bg="black",fg="white").grid(columnspan=2)
textentry2 = Entry(window,width=50,bg="white")
textentry2.grid(row=3,column=0,sticky=E,pady=10)
Button(window, text="Browse",width=6,command=click2).grid(row=3,column=1,sticky=E,pady=10)


# In[23]:


Label(window,text= "Select excel file",bg="black",fg="white").grid(columnspan=2)
textentry3 = Entry(window,width=50,bg="white")
textentry3.grid(row=5,column=0,sticky=E,pady=10)
Button(window, text="Browse",width=6,command=click3).grid(row=5,column=1,sticky=E,pady=10)
Button(window, text="Submit",width=6,command=click).grid(pady=10,columnspan=2)


# In[24]:


window.mainloop()




