import numpy as np
import cv2
import os
from PIL import Image, ImageEnhance
import time
from psd_tools import PSDImage
from itertools import combinations

def bool_flag(s):
    if s == '1' or s == 'True' or s == 'true':
        return True
    elif s == '0' or s == 'False' or s == 'false':
        return False
    msg = 'Invalid value "%s" for bool flag (should be 0/1 or True/False or true/false)'
    raise ValueError(msg % s)

def centroid(coord):
    '''
    returns centroid (x,y)
    '''
    [left,top,right,bottom] = coord
    center = [round((left+right+1)/2),round((bottom+top+1)/2)]
    
    return center

def extract_asset(asset):
    '''
    input:
    asset: asset to be extracted (for pasting) as numpy array
    output:
    overlay of extracted asset, mask of extracted asset
    '''
    #extracting the asset without alpha channel
    asset_int = asset[:,:,:3]
    #creating mask using the alpha channel
    mask_int = asset[:,:,3].copy()
    mask = np.zeros_like(asset_int)
    mask[:,:,0] = mask_int
    mask[:,:,1] = mask_int
    mask[:,:,2] = mask_int
    mask = mask/255
    
    return asset_int,mask

def centroid_track(center,creative,reshape):
    '''
    input:
    center: centroid of elements in the original creative (x,y)
    creative: original creative
    reshape: new size of the creative (w,h)
    output:
    new centroid in the resized creative (x,y)
    '''
    
    cen_track = np.zeros_like(creative[:,:,0])
    #create a square blob at the centroid 
    cen_track[center[1]-2:center[1]+3,center[0]-2:center[0]+3] =1
    cen_track = Image.fromarray(cen_track)
    cen_track = cen_track.resize(reshape)
    cen_track = np.array(cen_track)
    #track the blob in the resized array
    new = np.where(cen_track==1)
    new_center = [round((np.max(new[1])+np.min(new[1]))/2),round((np.max(new[0])+np.min(new[0]))/2)]
    
    return new_center

def backgroud_crop(asset,reshape,creative):
    '''
    input:
    asset: background asset
    reshape: target shape of the creative (w,h)
    creative: original creative
    output:
    background in the target shape
    '''
    
    width = min(asset.shape[1],creative.shape[1])
    height = min(asset.shape[0],creative.shape[0])
    
    asset_ar = round(width/height,2)
    target_ar = round(reshape[0]/reshape[1],2)
    
    #reshape and crop parameters for background resizing
    if asset_ar == target_ar:
        w = width
        h = height
        left = 0
        top = 0
    elif asset_ar < target_ar:
        w = width
        h = round(w/target_ar)
        left = 0
        top = round(height/2 - h/2)
    else:
        h = height
        w = round(h*target_ar)
        left = round(width/2 - w/2)
        top = 0
    
    #resizing the background maintaining the aspect ratio
    #background = cv2.resize(asset,(w,h),cv2.INTER_AREA)
    
    #cropping the target shape out of the resized background
    background = asset[top:top+h,left:left+w]
    
    return background, (w,h)

def new_box(new_center,asset):
    '''
    input:
    new_center: centroid of the element in the new creative
    asset: element of creative
    output:
    new rectangular coordinates: [left,top,right,bottom]
    '''
    
    left = round(new_center[0] - asset.shape[1]/2)
    top = round(new_center[1] - asset.shape[0]/2)
    right = left + asset.shape[1]
    bottom = top + asset.shape[0]
    
    return [left,top,right,bottom]

def bg2ele_checknresize(new_center, asset, creative, org_creative, margin=0, point_of_resize=0,fit=[0,0]):
    '''
    description: check element bounding box w.r.t to background and resize elements
    input:
    new_center: centroid of the element in the new creative
    asset: element to be resized
    creative: the background asset where the element is to be pasted
    org_creative: benchmark creative
    margin (default=0): margin to be maintained with boundary
    point_of_resize: point about which resizing has to be done
    -> 0 : centroid
    -> 1 : bottom left
    -> 2 : top left
    -> 3 : top right
    -> 4 : bottom right
    fit: asset fit to height/width of background
    output:
    resized asset
    its bounding box location in the new creative [left,top,right,bottom]
    '''
    original_ar = round(org_creative.shape[1]/org_creative.shape[0],2)
    target_ar = round(creative.shape[1]/creative.shape[0],2)
    
    w = asset.shape[1]
    h = asset.shape[0]
    
    if (point_of_resize == 0) & (fit[0] | fit[1]):
        point_of_resize =1
    
    #bounding box of the asset about the new centroid 
    new_coord = new_box(new_center,asset)
    #extension of the asset beyond the boundary of the background
    left_extension = int(margin*creative.shape[1]) - new_coord[0]
    top_extension = int(margin*creative.shape[0]) - new_coord[1]
    right_extension = new_coord[2] - creative.shape[1] + int(margin*creative.shape[1])
    bottom_extension = new_coord[3] - creative.shape[0] + int(margin*creative.shape[0])
    
    #resizing technique for resizing about centroid
    if point_of_resize == 0:
        if (left_extension > 0) | (top_extension > 0):
            w_int = w - left_extension
            h_int = int((w_int/w)*h)
            if (h - h_int) < top_extension:
                h_int = h - top_extension
                w_int = int((h_int/h)*w)
            #since the resizing is about the centroid
            new_w = w - 2*(w - w_int)
            new_h = h - 2*(h - h_int)
            #creating temporary array of the size (new_h,new_w)
            tmp = np.zeros((new_h,new_w))
            new_coord = new_box(new_center,tmp)
            w = new_w
            h = new_h
        
        right_extension = new_coord[2] - creative.shape[1] + int(margin*creative.shape[1])
        bottom_extension = new_coord[3] - creative.shape[0] + int(margin*creative.shape[0])
        
        if (right_extension > 0) | (bottom_extension > 0):
            w_int = w - right_extension
            h_int = round((w_int/w)*h)
            if (h - h_int) < bottom_extension:
                h_int = h - bottom_extension
                w_int = round((h_int/h)*w)
            #since the resizing is about the centroid
            new_w = w - 2*(w - w_int)
            new_h = h - 2*(h - h_int)
        else:
            new_w = w
            new_h = h
    
    #resizing technique for resizing about corner points
    else:
        #total extension of the asset beyond the boundary of the background in x and y direction
        x_extension = max(0,left_extension) + max(0,right_extension)
        y_extension = max(0,top_extension) + max(0,bottom_extension)
        

        #resizing the asset based on its extension in the x and y direction
        if (x_extension > 0) | (y_extension > 0):
            new_w = w - x_extension
            new_h = int((new_w/w)*h)
            if (h-new_h) < y_extension:
                new_h = h - y_extension
                new_w = int((new_h/h)*w)
        else:
            new_w = w
            new_h = h
    
    
    if fit[0]:
        new_w = creative.shape[1]
        new_h = round((new_w/w)*h)
                
    if fit[1]:
        new_h = creative.shape[0]
        new_w = round((new_h/h)*w)
    
    asset_int = Image.fromarray(asset)
    asset_int.thumbnail((new_w,new_h),Image.ANTIALIAS)
    new_w,new_h = asset_int.size
    asset_int = np.array(asset_int)
    
    
    '''
    if (new_w > creative.shape[1]) | (new_h > creative.shape[0]):
        if (fit[0] | fit[1]) & (point_of_resize == 1):
            if fit[0]:
                asset_int = asset_int[(new_h-creative.shape[0]):,:]
            if fit[1]:
                asset_int = asset_int[:,:creative.shape[1]]
        elif (fit[0] | fit[1]) & (point_of_resize == 2):
            if fit[0]:
                asset_int = asset_int[:creative.shape[0],:]
            if fit[1]:
                asset_int = asset_int[:,:creative.shape[1]]
        elif (fit[0] | fit[1]) & (point_of_resize == 3):
            if fit[0]:
                asset_int = asset_int[:creative.shape[0],:]
            if fit[1]:
                asset_int = asset_int[:,new_w-creative.shape[1]:]
        elif (fit[0] | fit[1]) & (point_of_resize == 3):
            if fit[0]:
                asset_int = asset_int[(new_h-creative.shape[0]):,:]
            if fit[1]:
                asset_int = asset_int[:,(new_w-creative.shape[1]):]
    '''
        
    if point_of_resize == 0:
        [left,top,right,bottom] = new_box(new_center,asset_int)
    
    elif point_of_resize == 1:
        if left_extension > 0:
            left = int(margin*creative.shape[1])
        else:
            left = new_coord[0]

        if bottom_extension > 0:
            bottom = creative.shape[0] - int(margin*creative.shape[0])
        else:
            bottom = new_coord[3]

        right = left + new_w
        top = bottom - new_h
        
    elif point_of_resize == 2:
        if left_extension > 0:
            left = int(margin*creative.shape[1])
        else:
            left = new_coord[0]

        if top_extension > 0:
            top = int(margin*creative.shape[0])
        else:
            top = new_coord[1]

        right = left + new_w
        bottom = top + new_h
        
    elif point_of_resize == 3:
        if right_extension > 0:
            right = creative.shape[1] - int(margin*creative.shape[1])
        else:
            right = new_coord[2]

        if top_extension > 0:
            top = int(margin*creative.shape[0])
        else:
            top = new_coord[1]

        left = right - new_w
        bottom = top + new_h
        
    else:
        if right_extension > 0:
            right = creative.shape[1] - int(margin*creative.shape[1])
        else:
            right = new_coord[2]

        if bottom_extension > 0:
            bottom = creative.shape[0] - int(margin*creative.shape[0])
        else:
            bottom = new_coord[3]

        left = right - new_w
        top = bottom - new_h 
        
    
    box = [left,top,right,bottom]
    
    
#    if (fit[0] | fit[1]) & ((new_w > creative.shape[1]) | (new_h > creative.shape[0])):
#        box = [0,0,creative.shape[1],creative.shape[0]]
        
    
    return asset_int, box

def ele2ele_checknresize(asset, box, new_center, background, mask_overlay, margin=10, point_of_resize=0):
    '''
    description: check element bounding box w.r.t to already pasted elements and resize elements
    input:
    box: bounding box of element to be pasted in the new creative [left,top,right,bottom] 
    asset: element to be pasted
    new_center: centroid of the asset to be pasted in the new creative
    background: background asset
    margin: minimum spacing b/w two elements to be maintained
    mask_overlay: mask of already pasted assets in the new creative
    point_of_resize: point about which resizing has to be done
    -> 0 : centroid
    -> 1 : bottom left
    -> 2 : top left
    -> 3 : top right
    -> 4 : bottom right
    output:
    resized asset
    its bounding box location in the new creative [left,top,right,bottom]
    '''
    
    new_w = asset.shape[1]
    new_h = asset.shape[0]
    asset_int = asset.copy()
    box_int = box.copy()
    #creating binary mask from mask overlay
    mask_overlay[mask_overlay<1] = 0.0
    mask_overlay[mask_overlay>=1] = 1.0
    
    while True:
        #creating asset mask overlay
        ext_asset, int_mask = extract_asset(asset_int)
        _,mask,_ = create_overlaynmask(background,ext_asset,int_mask,box_int)
        mask[mask<1] = 0.0
        mask[mask>=1] = 1.0
        #creating a dilated asset mask of a given margin
        new_mask = cv2.dilate(mask[:,:,0], cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (margin,margin)))
        #checking for intersection b/w asset and already pasted assets
        interArea = np.sum(cv2.bitwise_and(new_mask,mask_overlay[:,:,0]))

        if interArea == 0:
            break
        else:
            h_int = new_h - 5
            w_int = round((h_int/new_h)*new_w)
            new_h = h_int
            new_w = w_int
            #resizing the asset
            asset_int = Image.fromarray(asset_int)
            asset_int.thumbnail((new_w,new_h),Image.ANTIALIAS)
            new_w,new_h = asset_int.size
            asset_int = np.array(asset_int)
            
            #get the bounding box from the centroid and the resized asset
            if point_of_resize == 0:
                [left,top,right,bottom] = new_box(new_center,asset_int)
    
            elif point_of_resize == 1:
                left = box_int[0]
                bottom = box_int[3]
                right = left + new_w
                top = bottom - new_h

            elif point_of_resize == 2:
                left = box_int[0]
                top = box_int[1]
                right = left + new_w
                bottom = top + new_h

            elif point_of_resize == 3:
                right = box_int[2]
                top = box_int[1]
                left = right - new_w
                bottom = top + new_h

            else:
                right = box_int[2]
                bottom = box_int[3]
                left = right - new_w
                top = bottom - new_h 

            box_int = [left,top,right,bottom]

    return asset_int,box_int

def create_overlaynmask(creative, asset, mask, box):
    '''
    input:
    creative: the creative/background
    asset: the asset to be pasted
    mask: mask of the asset (numpy array)
    centroid: centroid location [x,y] of the asset to be pasted
    output:
    overlay, mask and layer of the asset
    '''
    overlay = np.zeros_like(creative)
    mask_int = np.zeros_like(creative,dtype=np.float32)
    
    [left,top,right,bottom] = box
    
    #creating the overlay and mask for pasting
    overlay[max(0,top):min(overlay.shape[0],bottom),max(0,left):min(overlay.shape[1],right)] = asset[max(0,top)-top:min(overlay.shape[0],bottom)-top,max(0,left)-left:min(overlay.shape[1],right)-left].copy()
    mask_int[max(0,top):min(overlay.shape[0],bottom),max(0,left):min(overlay.shape[1],right)] = mask[max(0,top)-top:min(overlay.shape[0],bottom)-top,max(0,left)-left:min(overlay.shape[1],right)-left].copy()
    
    #creating asset layer
    asset_layer = cv2.cvtColor(overlay,cv2.COLOR_BGR2BGRA)
    asset_layer[:,:,3] = 255*mask_int[:,:,0].copy()
    
    return overlay, mask_int, asset_layer

def paste_asset(overlay,mask_int,creative):
    '''
    input:
    overlay: asset overlay
    mask_int: asset mask
    output:
    creative with the pasted asset (numpy array)
    '''
    
    #pasting
    creative_int = overlay*mask_int + creative*(1-mask_int)
    creative_fin = creative_int.astype(np.uint8)
    
    return creative_fin

def groupasset(asset_lst,creative,box,src_path):
    '''
    asset_lst : list of asset to be grouped.
                order of elements in list --> order of pasting
    creative: benchmark creative
    box: list of center value of assets
    src_path: source path
    output:
    asset_fin: grouped asset
    '''
    
    overlay = np.zeros((creative.shape[0],creative.shape[1],4))
    alpha = np.zeros((creative.shape[0],creative.shape[1]),dtype=np.float32)
    lst = []
    #initialising bounding box of group asset
    if box == 0:
        gbbox = asset_lst[0].bbox
        [gleft,gtop,gright,gbottom] = [max(0,gbbox[0]),max(0,gbbox[1]),
                                       min(creative.shape[1],gbbox[2]),min(creative.shape[0],gbbox[3])]
    else:
        gcenter = box[0]
        gbbox = new_box(gcenter,np.array(Image.open(os.path.join(src_path,asset_lst[0].name+'.png'))))
        [gleft,gtop,gright,gbottom] = [max(0,gbbox[0]),max(0,gbbox[1]),
                                       min(creative.shape[1],gbbox[2]),min(creative.shape[0],gbbox[3])]
        
    for i,element in enumerate(asset_lst):
        if box == 0:
            asset = np.array(Image.open(os.path.join(src_path,element.name+'.png')))
            if asset.shape[2] == 3:
                asset = cv2.cvtColor(asset,cv2.COLOR_RGB2RGBA)
            bbox = element.bbox
            #detecting location of asset
            rect_box = bbox
            center = centroid(rect_box)
            rect_box = new_box(center,asset)
            
            lst.append(center)
            [left,top,right,bottom] = rect_box
            if left < gleft:
                gleft = left
            if top < gtop:
                gtop = top
            if right > gright:
                gright = right
            if bottom > gbottom:
                gbottom = bottom
        else:
            asset = np.array(Image.open(os.path.join(src_path,element.name+'.png')))
            if asset.shape[2] == 3:
                asset = cv2.cvtColor(asset,cv2.COLOR_RGB2RGBA)
            asset_center = box[i]
            rect_box = new_box(asset_center,asset)
            [left,top,right,bottom] = rect_box
            if left < gleft:
                gleft = left
            if top < gtop:
                gtop = top
            if right > gright:
                gright = right
            if bottom > gbottom:
                gbottom = bottom
        
        ext_asset, int_mask = extract_asset(asset)
        #creating the overlay and mask for pasting
        overlay_int,mask_int,_ = create_overlaynmask(creative,ext_asset,int_mask,rect_box)
        #pasting asset
        overlay[:,:,:3] = paste_asset(overlay_int,mask_int,overlay[:,:,:3])
        #creating alpha channel
        alpha += mask_int[:,:,0]
    
    #post processing
    alpha[alpha>1.0] = 1.0
    overlay[:,:,3] = 255*alpha
    asset_fin = overlay[max(0,gtop):min(creative.shape[0],gbottom),max(0,gleft):min(creative.shape[1],gright)]
    
    
    return asset_fin.astype(np.uint8),[gleft,gtop,gright,gbottom],lst

def overlap_check(assetA,assetB,creative,thres=0):
    '''
    Description: checking overlaping of assets based on their masks
    '''
    boxA = assetA.bbox
    boxB = assetB.bbox
    
    assetA_arr = np.array(assetA.composite())
    assetB_arr = np.array(assetB.composite())
    
    #mask of assetA
    ext_assetA, int_maskA = extract_asset(assetA_arr)
    _,maskA,_ = create_overlaynmask(creative,ext_assetA,int_maskA,boxA)
    maskA[maskA<1] = 0.0
    maskA[maskA>=1] = 1.0
    
    #mask of assetB
    ext_assetB, int_maskB = extract_asset(assetB_arr)
    _,maskB,_ = create_overlaynmask(creative,ext_assetB,int_maskB,boxB)
    maskB[maskB<1] = 0.0
    maskB[maskB>=1] = 1.0
    
    #checking intersection of mask
    interArea = np.sum(cv2.bitwise_and(maskA[:,:,0],maskB[:,:,0]))
    pctAreaA = interArea/np.sum(maskA[:,:,0])
    pctAreaB = interArea/np.sum(maskB[:,:,0])
    if (pctAreaA > thres) or (pctAreaB > thres):
        flagA = True
        flagB = False
    else:
        flagA = False
        if interArea != 0:
            flagB = True
        else:
            flagB = False
            
    return flagA,flagB
            
        
    
    
